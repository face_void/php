<!-- Выполните эти же действия, с помощью одного оператора echo. -->

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>5</title>
</head>
<body>
	<?php 
		$string1 = '"Славная осень! Здоровый, ядреный';
		$string2 = 'Воздух усталые силы бодрит;';
		$string3 = 'Лед неокрепший на речке студеной';
		$string4 = 'Словно как тающий сахар лежит."';
		$author = 'Н. А. Некрасов';

		echo "$string1<br>$string2<br>$string3<br>$string4<br>
		<span style = 'text-decoration: underline'>$author</span>";
	?>
</body>
</html>