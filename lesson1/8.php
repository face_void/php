<!-- Дан фрагмент кода:
<php
$x = 10;
$y = 15;
?>
Необходимо дописать несколько операций так, чтобы в итоге значения переменных
поменялись местами. При этом, использовать другие переменные запрещается. -->

<!DOCTYPE html>
<html>
<head>
	<title>8</title>
	<meta charset="utf-8">
</head>
<body>
	<?php 
		$x = 10;
		$y = 15;
		echo "x = $x, y = $y";
		$x += $y;
		$y = $x - $y;
		$x -= $y;
		echo " x = $x, y = $y";
	?>
</body>
</html>