<!-- С помощью оператора echo выведите на страницу:
a. Целочисленную переменную
b. Переменную дробного типа
c. Переменную булевского типа
d. Строковую переменную
e. Константу -->

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>1</title>
</head>
<body>
	<?php
		$int = 1;
		$float = 1.1;
		$bool = true;
		$string = 'Hi';
		define('CONSTANT', 5);

		echo $int.' ';
		echo $float.' ';
		echo $bool.' ';
		echo $string.' ';
		echo CONSTANT.' ';
	?>
</body>
</html>

