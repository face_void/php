<!-- Повторите вывод, заключив переменные в двойные кавычки (“). Посмотрите, что
получится. Объясните результат. -->

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>2</title>
</head>
<body>
	<?php 
		$int = 1;
		$float = 1.1;
		$bool = true;
		$string = 'Hi';
		define('CONSTANT', 5);

		echo "$int";
		echo "$float";
		echo "$bool";
		echo "$string";
		echo "CONSTANT";
	?>
</body>
</html>
