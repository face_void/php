<!-- Повторите вывод, заключив переменные в одинарные кавычки (‘). Посмотрите, что
получится. Объясните результат. -->

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>3</title>
</head>
<body>
	<?php 
		$int = 1;
		$float = 1.1;
		$bool = true;
		$string = 'Hi';
		define('CONST', 5);

		echo '$int';
		echo '$float';
		echo '$bool';
		echo '$string';
		echo 'CONST';
	?>
</body>
</html>
