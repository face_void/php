<!DOCTYPE html>
<html>
<head>
    <title>PHP уровень 2 - главная</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="theme/style.css">
</head>
<body>
<h1>PHP Уровень 2 - главная страница</h1>
<br>
<a href="editor.php">Консоль редактора</a>
<hr>
<h2>Список статей: </h2>
<ul>
    <?php foreach ($articles as $article): ?>
        <li><a href="article.php?id=<?php echo $article['id_article'] ?>"><?php
                echo $article['title'] ?></a></li>
    <?php endforeach ?>
</ul>
<hr>
</body>
</html>