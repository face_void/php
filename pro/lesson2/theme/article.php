<!DOCTYPE html>
<html>
<head>
    <title>PHP уровень 2 - просмотр статьи</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="theme/style.css">
</head>
<body>
    <h1>PHP Уровень 2</h1>
    <br>
    <a href="index.php">Главная</a> |
    <a href="editor.php">Консоль редактора</a> |
    <a href="edit.php?id=<?php echo $id_article; ?>">Редактировать</a>
<hr>
    <h2><?php echo $title; ?></h2>
    <p><?php echo $content; ?></p>
<hr>
</body>
</html>