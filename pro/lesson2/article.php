<?php

    require_once('startup.php');
    require_once('model.php');

    // подключаемся к БД
    startup();

    //получаем данные статьи
    if (!empty($_GET) && isset($_GET['id'])) {
        $article = articles_get($_GET['id']);
        $title = $article[0]['title'];
        $content = $article[0]['content'];
        $id_article = $article[0]['id_article'];
    } else
        //если каким-то образом не сработало, возарвщаемся на главную
        include('index.php');

    // кодировку
    header('Content-type: text/html; charset=utf-8');

    // вывод в шаблон
    include('theme/article.php');