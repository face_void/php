<?php

// список всех статей
function articles_all()
{
	// запрос
	$sql = 'SELECT * FROM `articles` ORDER BY id_article DESC';
	$result = mysqli_query(getDbConnect(), $sql);
	if (!$result) {
		die(mysqli_error());
	}

	// извлекаем из БД данные
	$rows = mysqli_num_rows($result);
	$articles = array();
	if (!$rows) {
		return $articles;
	}
	while ($row = mysqli_fetch_assoc($result)) {
		$articles[] = $row;
	}
	return $articles;
}

// получить конкретную статью
function articles_get($id_article)
{
    $sql = "SELECT * FROM `articles` WHERE id_article = '$id_article' ";
    $result = mysqli_query(getDbConnect(), $sql);
    if (!$result) {
        die(mysqli_error());
    }

    // извлекаем из БД данные
    $rows = mysqli_num_rows($result);
    $article = array();
    if (!$rows) {
        return $article;
    }
    while ($row = mysqli_fetch_assoc($result)) {
        $article[] = $row;
    }
    return $article;
}

// добавить статью
function articles_new($title, $content)
{
	// Подготовка
	$title = trim($title);
	$content = trim($content);

	// Проверка
	if ($title == '') {
		return false;
	}

	// Запрос
	$sql = "INSERT INTO `articles` (`title`, `content`) VALUES ('%s', '%s')";
	$query = sprintf($sql, sql_escape($title), sql_escape($content));

	$result = mysqli_query(getDbConnect(), $query);

	if (!$result) {
		die(mysqli_error());
	}
	return true;
}

// изменить статью
function articles_edit($id_article, $title, $content)
{
    // Подготовка
    $title = trim($title);
    $content = trim($content);

    // Проверка
    if ($title == '') {
        return false;
    }

    // Запрос
    $sql = "UPDATE `articles` SET title = '%s', content = '%s' WHERE id_article = '%u'";
    $query = sprintf($sql, sql_escape($title), sql_escape($content), $id_article);

    $result = mysqli_query(getDbConnect(), $query);

    if (!$result) {
        die(mysqli_error());
    }
    return true;
}

// удаление статьи
function articles_delete($id_article)
{
    $sql = "DELETE FROM `articles` WHERE id_article = '$id_article'";

    $result = mysqli_query(getDbConnect(), $sql);

    if (!$result) {
        die(mysqli_error());
    }
    return true;
}

// короткое описание статьи
function articles_intro($article)
{
	// TODO
	// $article - это ассоциативный массив, представляющий статью
}
