<?php
    require_once('startup.php');
    require_once('model.php');

    startup();

    define('DELETE', 'Удалить');

    //var_dump($_GET);
    //получаем данные о статье, которую редактируем
    if (!empty($_GET) && isset($_GET['id'])) {
        $article = articles_get($_GET['id']);
        $title = $article[0]['title'];
        $content = $article[0]['content'];
    }

    // Определяем переменные для шаблона
    $error = false;
    $success = false;

    //обработка запроса на удаление статьи
    if (!empty($_POST) && isset($_POST['del'])) {
        if (articles_delete($_GET['id'])) {
            die(header('Location: editor.php'));
        }
    }

    //обработка отправки формы редактирования
    if (!empty($_POST) && isset($_POST['title']) && isset($_POST['content'])) {
        if (articles_edit($_GET['id'], $_POST['title'], $_POST['content'])) {
            die(header('Location: editor.php'));
        }

        $title = $_POST['title'];
        $content = $_POST['content'];
        $error = true;
    }

    // кодировку
    header('Content-type: text/html; charset=utf-8');

    // вывод в шаблон
    include('theme/edit.php');


