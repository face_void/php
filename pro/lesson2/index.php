<?php
    require_once('startup.php');
    require_once('model.php');

    startup();

    $articles = articles_all();

    header('Content-type: text/html; charset=utf-8');

    include('theme/index.php');
