<h2>Редактирование статьи</h2>
<?php if ($error): ?>
    <b style="color:red">Заполните все поля!</b>
<?php endif; ?>
<form method="post">
    Название<sup style="color:red">*</sup>: <br>
    <input type="text" name="title" value="<?php echo $title ?>">
    <br><br>
    Содержание: <br>
    <textarea name="content"><?php echo $content?></textarea>
    <br>
    <input type="submit" value="Редактировать">
    <input type="submit" name="del" value="<?php echo DELETE ?>">
</form>
