<?php
//
// Конттроллер страницы чтения.
//
//include_once('m/model.php');

class C_Page extends C_Base
{
    //
    // Конструктор.
    //
    // главная страница
    public function action_index(){
        $articles = articles_all();
        $this->title = 'PHP уровень 2 - главная';
        $this->links = '<a href="index.php?act=editor">Консоль редактора</a>';
        $this->content = $this->Template('v/index.php', array('articles' => $articles));
    }
    // консоль редактора
    public function action_editor() {
        $articles = articles_all();
        $this->title = 'PHP уровень 2 - консоль редактора';
        $this->links = '<a href="index.php">Главная</a> | <b>Консоль редактора</b>';
        $this->content = $this->Template('v/editor.php', array('articles' => $articles));
    }
    // редактирование статьи
    public function action_edit(){
        define('DELETE', 'Удалить');

        $this->title = 'PHP уровень 2 - редактирование статьи';
        $this->links = "<a href=\"index.php\">Главная</a> | 
              <a href=\"index.php?act=editor\">Консоль редактора</a>";

        //получаем данные о статье, которую редактируем
        if (!empty($_GET) && isset($_GET['id'])) {
            $article = articles_get($_GET['id']);
            $article_title = $article[0]['title'];
            $article_content = $article[0]['content'];
        }

        $error = false;

        //обработка запроса на удаление статьи
        if (!empty($_POST) && isset($_POST['del'])) {
            if (articles_delete($_GET['id'])) {
                die(header('Location: index.php?act=editor'));
            }
        }

        //обработка отправки формы редактирования
        if (!empty($_POST) && isset($_POST['title']) && isset($_POST['content'])) {
            if (articles_edit($_GET['id'], $_POST['title'], $_POST['content'])) {
                die(header('Location: index.php?act=editor'));
            }

            $article_title = $_POST['title'];
            $article_content = $_POST['content'];
            $error = true;
        }

        $this->content = $this->Template('v/edit.php', array('title' => $article_title,
            'content' => $article_content,
            'error' => $error));
    }
    // просмотр статьи
    public function action_article() {
        if (!empty($_GET) && isset($_GET['id'])) {
            $article = articles_get($_GET['id']);
            $article_title = $article[0]['title'];
            $article_content = $article[0]['content'];
            $id_article = $article[0]['id_article'];
        } else
            //если каким-то образом не сработало, возарвщаемся на главную
            die (header('Location: index.php'));

        $this->title = 'PHP уровень 2 - просмотр статьи';
        $this->links = "<a href='index.php'>Главная</a> |
                  <a href='index.php?act=editor'>Консоль редактора</a> |
                  <a href='index.php?id=$id_article&act=edit'>Редактировать</a>";

        $this->content = $this->Template('v/article.php', array('title' => $article_title, 'content' => $article_content));
    }
    // новая статья
    public function action_new() {
        $this->title = '';
        $this->content = '';
        $this->error = false;

// Обработка отправки формы
        if (!empty($_POST) && isset($_POST['title']) && isset($_POST['content'])) {
            // успешно данные добавлены, редирект
            if (articles_new($_POST['title'], $_POST['content'])) {
                die(header('Location: index.php?act=editor'));
            }
            $this->title = $_POST['title'];
            $this->content = $_POST['content'];
            $this->error = true;
        }

// кодировку
        header('Content-type: text/html; charset=utf-8');

// вывод в шаблон
        include('v/new.php');
    }
}