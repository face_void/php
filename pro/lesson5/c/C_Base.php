<?php

    abstract class C_Base extends C_Controller {

        protected $title;		// заголовок страницы
        protected $content;		// содержание страницы
        protected $links;

        protected function before() {
            $this->title = 'Новостной блог';
            $this->content = '';
            $this->links = '';
        }

    //
    // Генерация базового шаблона
    //
        public function render() {
            $vars = array('title' => $this->title, 'content' => $this->content, 'links' =>$this->links);
            $page = $this->Template('v/main.php', $vars);
            echo $page;
        }
    }