<?php

function __autoload($classname){
    include_once("c/$classname.php");
}

include_once ('m/model.php');

startup();

$action = 'action_';
$action .= (isset($_GET['act'])) ? $_GET['act'] : 'index';
$controller = new C_Page();
$controller->Request($action);