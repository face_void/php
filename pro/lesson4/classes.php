<?php

class Article
{

	protected $id;
	protected $title;
	protected $content;
	protected $preview;
	
	function __construct($id, $title, $content)
	{
		$this->id = $id;
		$this->title = $title;
		$this->content = $content;
		$this->preview = substr($content, 0, 15).'...';
	}
	
	//  Функция для вывода статьи
	function view()
	{
		echo "<h1>$this->title</h1><p>$this->content</p>";
	}

	function GetId() {
	    return $this->id;
    }
}

class NewsArticle extends Article
{
	var $datetime;

	function __construct($id, $title, $content)
	{
		parent::__construct($id, $title, $content);
		$this->datetime = time();
	}
	
	//  Функция для вывода статьи
	function view()
	{
		echo "<h1>$this->title</h1><span style='color: red'>".
				strftime('%d.%m.%y', $this->datetime).
				" <b>Новость</b></span><p>$this->content</p>";
	}
}

class CrossArticle extends Article
{
	var $source;
	
	function __construct($id, $title, $content, $source)
	{
		parent::__construct($id, $title, $content);
		$this->source = $source;
	}

	function view()
	{
		parent::view();
		echo '<small>'.$this->source.'</small>';
	}
}

class ImageArticle extends Article
{
    var $image;

    function __construct($id, $title, $content, $imageUrl)
    {
        parent::__construct($id, $title, $content);
        $this->imsge = $imageUrl;
    }

    function view()
    {
        parent::view();
        echo "<img src='$this->image'>";
    }
}

class ArticleList
{
	var $alist;
	
	function add(Article $article)
	{
		$this->alist[] = $article;
	}
	
	//  Вывод статей
	function view()
	{
		foreach($this->alist as $article)
		{
			$article->view();
			echo '<hr />';
		}
	}

	// удаление статьи по её id
    function delete($id) {
	    foreach ($this->alist as $key => $article) {
	        if ($article->GetId() == $id) {
	            unset($this->alist[$key]);
	            break;
            }
        }
    }
}

class ArticleListReverse extends ArticleList
{
    function view(){
        $this->alist = array_reverse($this->alist);
        parent::view();
    }
}

?>
