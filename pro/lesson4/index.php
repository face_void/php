<?php
require_once 'classes.php';

$articleList = new ArticleListReverse();

/* в цикле создаются по 3 экземпляра каждого класса
и добавляются в общий список статей */

for ($i=1; $i<=3; $i++) {
    ${'newsArticle'.$i} = new NewsArticle($i, "Заголовок$i", "Content$i");
    ${'crossArticle'.$i} = new CrossArticle($i, "Заголовок$i", "Content$i", "Источник$i");
    $articleList->add(${'newsArticle'.$i});
    $articleList->add(${'crossArticle'.$i});
}

$articleList->delete(1);
$articleList->view();

