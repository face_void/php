<?php
    include_once ('dbconnect.php');
?>

<!-- Создать базу и таблицу для хранения статей.
Написать пять простых скриптов:
1) index.php – выбока всех записей
2) article.php – выборка одной записи
3) add.php – добавление записи
4) delete.php – удаление одной записи
5) edit.php – изменений одной записи
В скриптах 2,4 и 5 номер статьи передаётся через GET-параметр.
Скрипты 3 и 5 не принимают название и содержание статьи от пользователя – оно просто
жёстко прописано в коде.
Данные скрипты являются отдельными несвязанными между собой файлами. -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Выборка всех записей</title>
</head>
<body>
    <form method="post">
        <input type="submit" name="paste" value="Вывести все статьи">
    </form>
</body>
</html>

<?php
    if ($_POST) {
        $result = mysqli_query($link, "SELECT * FROM `articles`");
        $articles = array();
        while ($row = mysqli_fetch_assoc($result))
            $articles[] = $row;
        foreach ($articles as $article) {
            echo "{$article['content']}<br>";
        }
    }
?>