<?php
    require_once('startup.php');
    require_once('model.php');

    // подключаемся к БД
    startup();

    //получаем данные статьи
    if (!empty($_GET) && isset($_GET['id'])) {
        $article = articles_get($_GET['id']);
        $article_title = $article[0]['title'];
        $article_content = $article[0]['content'];
        $id_article = $article[0]['id_article'];
    } else
        //если каким-то образом не сработало, возарвщаемся на главную
        include('index.php');

    $title = 'PHP уровень 2 - просмотр статьи';
    $links = "<a href='index.php'>Главная</a> |
                  <a href='editor.php'>Консоль редактора</a> |
                  <a href='edit.php?id=$id_article'>Редактировать</a>";

    $content = template('theme/article.php', array('title' => $article_title, 'content' => $article_content));

    $page = template('theme/main.php', array(
        'title' => $title,
        'links' => $links,
        'content' => $content
    ));

    echo $page;