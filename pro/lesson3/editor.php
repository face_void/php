<?php

    require_once('startup.php');
    require_once('model.php');

    // подключаемся к БД
    startup();

    // извлекаем все статьи
    $articles = articles_all();
    $title = 'PHP уровень 2 - консоль редактора';
    $links = '<a href="index.php">Главная</a> | <b>Консоль редактора</b>';

    $content = template('theme/editor.php', array('articles' => $articles));
    $page = template('theme/main.php', array('title' => $title,
                                             'links' => $links,
                                             'content' => $content));
    echo $page;
