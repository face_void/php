<?php
    require_once('startup.php');
    require_once('model.php');

    startup();

    define('DELETE', 'Удалить');

    $title = 'PHP уровень 2 - редактирование статьи';
    $links = "<a href=\"index.php\">Главная</a> | 
              <a href=\"editor.php\">Консоль редактора</a>";

    //получаем данные о статье, которую редактируем
    if (!empty($_GET) && isset($_GET['id'])) {
        $article = articles_get($_GET['id']);
        $article_title = $article[0]['title'];
        $article_content = $article[0]['content'];
    }

    $error = false;
    $success = false;

    //обработка запроса на удаление статьи
    if (!empty($_POST) && isset($_POST['del'])) {
        if (articles_delete($_GET['id'])) {
            die(header('Location: editor.php'));
        }
    }

    //обработка отправки формы редактирования
    if (!empty($_POST) && isset($_POST['title']) && isset($_POST['content'])) {
        if (articles_edit($_GET['id'], $_POST['title'], $_POST['content'])) {
            die(header('Location: editor.php'));
        }

        $article_title = $_POST['title'];
        $article_content = $_POST['content'];
        $error = true;
    }

    $content = template('theme/edit.php', array('title' => $article_title,
                                                'content' => $article_content,
                                                'error' => $error));

    $page = template('theme/main.php', array('title' => $title,
                                             'links' => $links,
                                             'content' => $content));

    echo $page;



