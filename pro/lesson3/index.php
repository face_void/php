<?php
    require_once('startup.php');
    require_once('model.php');

    startup();

    $articles = articles_all();

    $title = 'PHP уровень 2 - главная';
    $links = '<a href="editor.php">Консоль редактора</a>';

    $content = template('theme/index.php', array('articles' => $articles));
    $page = template('theme/main.php', array('title' => $title,
                                             'links' => $links,
                                             'content' => $content));
    echo $page;

