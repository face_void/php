<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="theme/style.css">
</head>
<body>
    <h1><?php echo $title ?></h1>
    <br>
    <?php echo $links ?>
    <hr>
    <?php echo $content ?>
    <hr>
</body>
</html>