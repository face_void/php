<?php
    include_once ('connection.php');
?>

<!-- Создать галерею изображений, состоящую из двух страниц:
index.php – просмотр всех фотографий (уменьшенных копий);
photo.php – просмотр конкретной фотографии (изображение оригинального размера).
В базе данных создать таблицу, в которой будет храниться информация о картинках. -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <title>Галерея фотографий</title>
</head>
<body>
<form method="post" enctype="multipart/form-data">
    <input type="hidden" name="action" value="upload">
    <input type="file" name="uplfile">
    <input type="submit" value="Загрузить файл">
</form>

<?php
    //загрузка нового изображения в папку img и занесение информации о ней в базу данных
    $uplDir = './img/';
    $uplFileName = basename($_FILES['uplfile']['name']);
    $uplFilePath = $uplDir.$uplFileName;
    if (count($_POST)) {
        move_uploaded_file($_FILES['uplfile']['tmp_name'], $uplFilePath);
        $res = mysqli_query($link, "INSERT INTO `images` (id, url) VALUES ('$uplFileName', '$uplFilePath')");
        /* В таблице содержится 2 столбца: id это имя картинки, которую мы загрузили, является первичным
        ключом; url это полный путь до картинки */
    }

    //функция подсчета переходов по картинке
    function clickCount($link) {

    }
?>

<div id="wrapper">
    <?php
        $result = mysqli_query($link, "SELECT * FROM `images` ORDER BY clicks DESC");
        $images = array();
        if ($result) {
            while ($row = mysqli_fetch_assoc($result))
                $images[] = $row;
        }
        foreach ($images as $image) {
            echo "<a class='image' href='photo.php?img={$image['id']}'>"; //в качестве параметра передаём имя картинки
            echo "<img src='{$image['url']}'>";
            echo "</a>";
        }
    ?>
</div>
</body>
</html>