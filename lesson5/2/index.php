<?php
    include_once('settings.php');
?>

<!-- Создайте три css файла с разными стилями. Затем сделайте страничку setting.php,
на которой пользователь сможет выбрать себе один из вариантов оформления
сайта. Информация о стиле сохраняется в куках и затем используется при показе
страничек. -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="css/<?php echo $style?>" rel="stylesheet" type="text/css">
    <title>Главная страница</title>
</head>
<body>
    <h1>Выберете цвет текста:</h1>
    <a href="a.php">Страница А</a><br>
    <a href="b.php">Страница Б</a>
    <form action="index.php" method="post">
        <input type="submit", name="color", value="<?php echo COLOR_RED?>">
        <input type="submit", name="color", value="<?php echo COLOR_BLUE?>">
        <input type="submit", name="color", value="<?php echo COLOR_BLACK?>">
    </form>
</body>