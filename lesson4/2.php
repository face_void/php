<!-- Превратите получившийся сумматор в калькулятор с четырьмя операциями:
сложение, вычитание, умножение, деление. Не забудьте обработать деление на
ноль!
Выбор операции можно осуществлять с помощью тега <select>. -->

<?php
if(isset($_POST['a']) && isset($_POST['b']))
    switch ($_POST['math'][0]) {
        case '+':
            $result = $_POST['a'] + $_POST['b'];
            break;
        case '-':
            $result = $_POST['a'] - $_POST['b'];
            break;
        case '*':
            $result = $_POST['a'] * $_POST['b'];
            break;
        case '/':
            if ($_POST['b'] == 0)
                $result = 'Деление на 0!';
            else
                $result = $_POST['a'] / $_POST['b'];
            break;
    }
else
    $result = "";
?>
<html>
<head>
    <title>Калькулятор</title>
</head>
<body>
<form method="post">
    <input type="text" name="a" />
    <select size="1" name="math[]">
        <option value="+">+</option>
        <option value="-">-</option>
        <option value="*">*</option>
        <option value="/">/</option>
    </select>
    <input type="text" name="b" />
    <input type="submit" value="=" />
    <?php echo $result; ?>
</form>
</body>
</html>