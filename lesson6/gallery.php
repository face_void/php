<!-- Создайте галерею фотографий. Она должна состоять всего из одной странички, на
которой пользователь видит все картинки в уменьшенном виде и форму для загрузки
нового изображения. При клике на фотографию она должна открыться в браузере в новой
вкладке. Размер картинок можно ограничивать с помощью свойства width. -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <title>Галерея фотографий</title>
</head>
<body>
    <form method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" value="upload">
        <input type="file" name="uplfile">
        <input type="submit" value="Загрузить файл">
    </form>

    <?php
        $upldir = './img/';
        $uplfile = $upldir.basename($_FILES['uplfile']['name']);
        if (count($_POST)) {
            if (move_uploaded_file($_FILES['uplfile']['tmp_name'], $uplfile)) {
                echo 'успех';
            }
        }
    ?>

    <div id="wrapper">
    <?php
        $images = scandir('img/');
        $dir = 'img/';
        for ($i = 2; $i < count($images); $i++) {
            $imgPath = $dir.$images[$i];
            echo "<a class='image' href='$imgPath' target='_blank'>";
            echo "<img src='$imgPath'>";
            echo "</a>";
        }
    ?>
    </div>
</body>
</html>