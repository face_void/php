<!-- Реализуйте основные 4 арифметические операции (+, -, *, %) в виде функций с
двумя параметрами. Обязательно используйте оператор return.
Реализуйте функцию с тремя параметрами: function mathOperation($arg1, $arg2,
$operation), где $arg1, $arg2 – значения аргументов, $operation – строка с названием
операции. В зависимости от переданного значения операции выполните одну из
арифметических операций (используйте функции из пункта 4) и верните
полученное значение (используйте switch). -->

<html>
<head>
    <meta charset = 'utf-8'>
    <title>3-4</title>
</head>
<body>
<?php
    function sum($a, $b) {
        return $a + $b;
    }

    function sub($a, $b) {
        return $a - $b;
    }

    function mul($a, $b) {
        return $a * $b;
    }

    function remain($a, $b) {
        return $a % $b;
    }

    function mathOperation($arg1, $arg2, $operation) {
        switch ($operation) {
            case 'sum':
                echo sum($arg1, $arg2);
                break;
            case 'sub':
                echo sub($arg1, $arg2);
                break;
            case 'mul':
                echo mul($arg1, $arg2);
                break;
            case 'remain':
                echo remain($arg1, $arg2);
                break;
        }
    }

    $a = 10;
    $b = 20;
    $op = 'mul';
    mathOperation($a, $b, $op);
?>
</body>
</html>