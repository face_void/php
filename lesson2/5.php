<!-- С помощью рекурсии организуйте функцию возведения числа в степень. Формат:
function power($val, $pow), где $val – заданное число, $pow – степень. -->

<html>
<head>
    <meta charset = 'utf-8'>
    <title>5</title>
</head>
<body>
<?php
    $a = 2;
    $power = 4;
    echo power($a, $power);

    function power($val, $pow) {
        if ($pow == 1)
            return $val;
        else
            return $val * power($val, $pow-1);

    }
?>
</body>
</html>