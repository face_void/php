<!-- Объявите две целочисленные переменные $a и $b и задайте им произвольные
начальные значения. Затем напишите скрипт, который работает по следующему
принципу:
a. если $a и $b положительные, выведите их разность;
b. если $а и $b отрицательные, выведите их произведение;
c. если $а и $b разных знаков, выведите их сумму.
Ноль можно считать положительным числом. -->

<html>
<head>
    <meta charset = 'utf-8'>
    <title>1</title>
</head>
<body>
    <?php
        $a = -3;
        $b = 5;
        if ($a >= 0 && $b >=0) {
            echo $a - $b;
        } elseif ($a < 0 && $b < 0) {
            echo $a * $b;
        } else {
            echo $a + $b;
        }
    ?>
</body>
</html>