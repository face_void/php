<!-- Напишите функцию, которая вычисляет текущее время и возвращает его в формате
с правильными склонениями, например:
22 часа 15 минут
21 час 43 минуты
итд.
Подсказка: часы и минуты можно узнать с помощью встроенной функции PHP –
date. -->

<html>
<head>
    <meta charset = 'utf-8'>
    <title>6</title>
</head>
<body>
<?php
    if (date('H') >= 10 && date('H') <= 20)
        echo (date(H)).' часов ';
    else {
        switch (date('H') % 10) {
            case 1:
                echo (date(H)) . ' час ';
                break;
            case 2:
            case 3:
            case 4:
                echo (date(H)) . ' часа ';
                break;
            default:
                echo (date(H)) . ' часов ';
                break;
        }
    }

    if (date('i') >= 10 && date('i') <= 20)
        echo (date(i)).' минут';
    else {
        switch (date(i) % 10) {
            case 1:
                echo (date(i)) . ' минута';
                break;
            case 2:
            case 3:
            case 4:
                echo (date(i)) . ' минуты';
                break;
            default:
                echo (date(i)) . ' минут';
                break;
        }
    }
?>
</body>
</html>