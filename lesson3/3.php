<!-- Выведите с помощью цикла for числа от 0 до 9, НЕ
используя тело цикла. То есть выглядеть должно вот так:
for(…){// здесь пусто} -->

<html>
<head>
    <meta charset = 'utf-8'>
    <title>3</title>
</head>
<body>
<?php
    function incEcho($i) {
        echo $i.' ';
        return $i;
    }
    for ($i = 0; ($i = incEcho($i)) < 9; $i++)
?>
</body>
</html>