<!-- Объедините две ранее написанные функции в одну, которая получает строку на
русском языке, производит транслитерацию и замену пробелов на подчеркивания
(аналогичная задача решается при конструировании url-адресов на основе названия
статьи в блогах). -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>8</title>
</head>
<body>
    <?php
        $alphabet = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ж' => 'zh', 'з' => 'z', 'и' => 'i',
            'й' => 'y', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's',
            'т' => 't', 'у' => 'u', 'ф' => 'f',
            'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sh', 'ъ' => '',
            'ы' => 'i', 'ь' => '\'', 'э' => 'e',
            'ю' => 'yu', 'я' => 'ya'
        );
        $str = 'без бутылки не разберешься';
        echo urlConstruct($str, $alphabet);

        function urlConstruct($str, $alphabet, $encoding = 'UTF-8') {
            $strlen = mb_strlen($str);

            while ($strlen) {
                $array[] = mb_substr($str, 0, 1, $encoding);
                $str = mb_substr($str, 1, $strlen, $encoding);
                $strlen = mb_strlen($str, $encoding);
            }
            //var_dump($array);

            foreach ($array as $letter) {
                foreach ($alphabet as $initLet => $transLet) {
                    if ($letter == $initLet) {
                        $letter = $transLet;
                        continue;
                    }
                }
                $result[] = $letter;
            }
            $transStr = implode('', $result);
            return $resultStr = implode('_', explode(' ', $transStr));
        }
    ?>
</body>
</html>