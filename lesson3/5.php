<!-- Повторите предыдущее задание, но выводите на экран
только города, начинающиеся с буквы «К». -->

<html>
<head>
    <meta charset = 'utf-8'>
    <title>5</title>
</head>
<body>
<?php
$areas = array(
    "Московская область" => array(
        "Москва", "Зеленоград", "Клин"
    ),
    "Ленинградская область" => array (
        "Санкт-Петербург", "Всеволожск", "Павловск", "Кронштадт"
    ),
    "Самарская область" => array (
        "Самара", "Новокуйбышевск", "Сызрань", "Кинель"
    )
);

$encoding = 'UTF-8';

foreach ($areas as $area) {
    foreach ($area as $num => $city) {
        $letter = mb_substr($city, 0, 1, $encoding);
        if ($letter == "К")
            echo $city;
    }
    echo "<br>";
}
?>
</body>
</html>