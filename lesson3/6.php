<!-- Объявите массив, индексами которого являются буквы русского языка, а
значениями – соответствующие латинские буквосочетания (‘а’=> ’a’, ‘б’ => ‘b’, ‘в’
=> ‘v’, ‘г’ => ‘g’, …, ‘э’ => ‘e’, ‘ю’ => ‘yu’, ‘я’ => ‘ya’).
Напишите функцию транслитерации строк. -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>6</title>
</head>
<body>
    <?php
        $alphabet = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ж' => 'zh', 'з' => 'z', 'и' => 'i',
            'й' => 'y', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's',
            'т' => 't', 'у' => 'u', 'ф' => 'f',
            'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sh', 'ъ' => '',
            'ы' => 'i', 'ь' => '\'', 'э' => 'e',
            'ю' => 'yu', 'я' => 'ya'
        );

        function translit($str, $alphabet, $encoding = 'UTF-8') {
            $strlen = mb_strlen($str);

            while ($strlen) {
                $array[] = mb_substr($str, 0, 1, $encoding);
                $str = mb_substr($str, 1, $strlen, $encoding);
                $strlen = mb_strlen($str, $encoding);
            }
            //var_dump($array);

            foreach ($array as $letter) {
                foreach ($alphabet as $initLet => $transLet) {
                    if ($letter == $initLet) {
                        $letter = $transLet;
                        continue;
                    }
                }
                $resultWord[] = $letter;
            }
            return implode('', $resultWord);
        }

        echo translit('без бутылки не разберешься', $alphabet);
    ?>
</body>
</html>